FROM debian:stretch
RUN echo "deb http://deb.debian.org/debian stretch-backports main contrib non-free" >> /etc/apt/sources.list 
RUN apt-get update
RUN apt-get -t stretch-backports install -y redis-server redis-tools
COPY redis.conf /etc/redis/redis.conf
CMD /usr/bin/redis-server /etc/redis/redis.conf
EXPOSE 6379
EXPOSE 16379
