Requirements needed to run the project:
docker-ce, ansible, git

How to use:
Checkout, go to the repo dir and run :
ansible-playbook redis.yml

To check te cluster status run:
sudo docker exec -it $containerid sh -c "redis-cli cluster info "

Warning:
Not intended for use in production.
This projects is made to get a grip 
with docker und redis since I never 
used them before.

